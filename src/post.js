const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostSchema = new Schema({
	title: {
		type: String
	},
	content: String,
});

module.exports = PostSchema;