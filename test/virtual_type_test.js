const assert = require('assert');
const User = require('../src/user');


describe('Virtual types', () => {
	it('postCount return number of posts', (done) => {
		const joe = new User({
			name: 'Joe',
			salary: 100000,
			posts: [{ title: 'Post 1'}, { title: 'Post 2'}]
		});
		
		joe.save()
			.then(() => User.findOne({name: 'Joe'}))
			.then((user) => {
				assert(joe.postCount === 2);
				done();
			});
	});
});