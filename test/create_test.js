const assert = require('assert');
const User = require('../src/user');

describe('Creating records', function() {
	// this.timeout(3000);
	it('saves a user', (done) => {
		joe = new User({ name: 'Joe', salary: 20000, age: 20, address: 'New Zeland, Oakland, 7th ave, 4'});
		
		
		joe.save()
			.then(() => {
				assert(joe.isNew === false);
				done();
			});
	});
});