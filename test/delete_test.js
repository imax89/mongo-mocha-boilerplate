const assert = require('assert');
const User = require('../src/user');

describe('Deleting a user', () => {
	
	let joe;
	
	beforeEach((done) => {
		joe = new User({ name: 'Joe', salary: 20000, age: 20, address: 'New Zeland, Oakland, 7th ave, 4'});
		joe.save().then(() => done());
	});
	
	it('model instance remove', (done) => {
		joe.remove()
			.then(() => User.findOne({ name: 'Joe' }))
			.then((user) => {
				assert(Object.is(user, null));
				done();
			});
	});
	
	it('class method remove', (done) => {
		User.remove({ name: 'Joe'})
			.then(() => User.findOne({ name: 'Joe' }))
			.then((user) => {
				assert(Object.is(user, null));
				done();
			});
	});
	
	it('class method findOneAndRemove', (done) => {
		User.findOneAndRemove({ name: 'Joe'})
			.then(() => User.findOne({ name: 'Joe' }))
			.then((user) => {
				assert(Object.is(user, null));
				done();
			});
	});
	
	it('class method findByIdAndRemove', (done) => {
		User.findByIdAndRemove(joe._id)
			.then(() => User.findOne({ name: 'Joe' }))
			.then((user) => {
				assert(Object.is(user, null));
				done();
			});
	});
	
});