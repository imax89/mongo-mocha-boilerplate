const assert = require('assert');
const User = require('../src/user');


describe('Subdocuments', () => {
	it('can create a subdocument', (done) => {
		const joe = new User({
			name: 'Joee',
			posts: [
				{
					title: 'Post title'
				}
			]
		});
		
		joe.save()
			.then(() => User.findOne({name: 'Joee'}))
			.then((user) => {
				assert(user.posts[0].title === 'Post title');
				done();
			});
		
	});
	
	
	it('Can add subdocs to an existing record', (done) => {
		const name = 'John';
		
		const john = new User({
			name: name,
			posts: []
		});
		
		const postname = 'New post';
		
		john.save()
			.then(() => User.findOne({name: name}))
			.then((user) => {
				user.posts.push({title: postname});
				return user.save();
			})
			.then(() => User.findOne({name: name}))
			.then((user) => {
				assert(user.posts[0].title === postname);
				done();
			});
	});
	
	it('Can delete an existing subdoc', (done) => {
		const name = 'John';
		const postname = 'New post';
		
		const john = new User({
			name: name,
			posts: [{title: postname}]
		});
		
		john.save()
			.then(() => User.findOne({name: name}))
			.then((user) => {
				user.posts[0].remove();
				return user.save();
			})
			.then(() => User.findOne({name: name}))
			.then((user) => {
				assert(user.posts.length === 0);
				done();
			});
	});
	
	
});