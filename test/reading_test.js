const assert = require('assert');
const User = require('../src/user');

describe('Reading users out of the database', () => {
	
	let joe;
	
	beforeEach((done) => {
		joe = new User({ name: 'Joe', salary: 20200, age: 20, address: 'New Zeland, Oakland, 7th ave, 4'});
		joe.save()
			.then(() => done());
	});
	
	it('finds all users with a name of Joe', (done) => {
		User.find({ name: 'Joe' })
			.then((users) => {
				assert(users[0]._id.toString() === joe._id.toString());
				done();
			});
	});
	
	it('find a user with a particular ID', (done) => {
		User.findOne({ _id: joe._id })
			.then((user) => {
				assert(user.name === 'Joe');
				done();
			});
	});
});